%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                         Grupo 62 N.56564 Pedro Caetano
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-------------------------------------------------------------------------------
%
% predicados para criar os movimentos
%
%-------------------------------------------------------------------------------
mov(c,(Linha,Coluna),(c,Nova_linha,Coluna)):-Nova_linha is Linha-1.
mov(b,(Linha,Coluna),(b,Nova_linha,Coluna)):-Nova_linha is Linha+1.
mov(e,(Linha,Coluna),(e,Linha,Nova_coluna)):-Nova_coluna is Coluna-1.
mov(d,(Linha,Coluna),(d,Linha,Nova_coluna)):-Nova_coluna is Coluna+1.

%-------------------------------------------------------------------------------
%
% predicado para o calculo dos movimentos possiveis
%
%-------------------------------------------------------------------------------
movs_possiveis(Lab,Pos_atual,Movs,Poss):-
	paredes(Lab,Pos_atual,Paredes),
	movs_possiveis(Paredes,Pos_atual,Movs,[c,b,e,d],Poss).
movs_possiveis(_,_,_,[],[]).
movs_possiveis(Paredes,Pos_atual,Movs,[Mov_teste|Resto_Movs],[Mov|Resto_Poss]):-
	\+existe_parede(Mov_teste,Paredes),
	mov(Mov_teste,Pos_atual,Mov),
	\+posicao_visitada(Movs,Mov),
	!,
	movs_possiveis(Paredes,Pos_atual,Movs,Resto_Movs,Resto_Poss).
movs_possiveis(Paredes,Pos_atual,Movs,[Mov_teste|Resto_Movs],Resto_Poss):-
	movs_possiveis(Paredes,Pos_atual,Movs,Resto_Movs,Resto_Poss).

%-------------------------------------------------------------------------------
%
% predicado auxiliar para verificar a existencia de uma parede.
%
%-------------------------------------------------------------------------------
existe_parede(X,[X|_]).
existe_parede(Z,[_|Y]):-
	existe_parede(Z,Y).

%-------------------------------------------------------------------------------
%
% predicado auxiliar para verificar se uma posicao de um labirinto (X,Y)
% foi visitada dada uma lista de movimentos
%
%-------------------------------------------------------------------------------
posicao_visitada([(_,X,Y)|_],(_,X,Y)).
posicao_visitada([_|Resto_movs],Mov):-
	posicao_visitada(Resto_movs,Mov).

%-------------------------------------------------------------------------------
%
% predicado auxiliar para obter as paredes de uma posicao (X,Y).
%
%-------------------------------------------------------------------------------
paredes([[Lista_paredes|_]|_],(1,1),Lista_paredes).
paredes([_|Resto_linhas],(X,Y),Lista_paredes):-
	X>1,
	Y>1,
	Novo_X is X-1,
	paredes(Resto_linhas,(Novo_X,Y),Lista_paredes).
paredes([[_|Resto_colunas]|Resto_Linhas],(1,Y),Lista_paredes):-
	Y>1,
	Novo_Y is Y-1,
	paredes([Resto_colunas|Resto_Linhas],(1,Novo_Y),Lista_paredes).

%-------------------------------------------------------------------------------
%
%predicado para calcular a distancia entre duas celulas do labirinto
%
%-------------------------------------------------------------------------------
distancia((L1,C1),(L2,C2),Dist):-Dist is abs(L1-L2)+abs(C1-C2).

%-------------------------------------------------------------------------------
%
% predicado para ordenar os movimentos de acordo com a distancia ao inicio e ao
% fim
%
%-------------------------------------------------------------------------------

ordena_poss(Movs,Poss_ord,Pos_inicial,Pos_final):-
	ordena_poss(Movs,Poss_ord,[],Pos_inicial,Pos_final).
ordena_poss([],Aux,Aux,_,_).
ordena_poss([P|R],Poss_ord,Aux,Pos_inicial,Pos_final):-
	insert(P,Aux,Novo_aux,Pos_inicial,Pos_final),
	ordena_poss(R,Poss_ord,Novo_aux,Pos_inicial,Pos_final).
insert(P,[],[P],_,_).
insert((Mov,X,Y),[(Mov1,X1,Y1)|R],[(Mov,X,Y),(Mov1,X1,Y1)|R],_,Pos_final):-
	distancia((X,Y),Pos_final,Dist_final),
	distancia((X1,Y1),Pos_final,Dist_final1),
	Dist_final < Dist_final1,
	!.
insert((Mov,X,Y),[(Mov1,X1,Y1)|R],[(Mov,X,Y),(Mov1,X1,Y1)|R],Pos_inicial,Pos_final):-
	distancia((X,Y),Pos_final,Dist_final),
	distancia((X1,Y1),Pos_final,Dist_final1),
	Dist_final =:= Dist_final1,
	distancia((X,Y),Pos_inicial,Dist_inicial),
	distancia((X1,Y1),Pos_inicial,Dist_inical1),
	Dist_inicial > Dist_inical1,
	!.
insert(Mov,[Mov1 | R],[Mov1|Novo_aux],Pos_inicial,Pos_final):-
	insert(Mov,R,Novo_aux,Pos_inicial,Pos_final).
%-------------------------------------------------------------------------------
%
%predicado para resolver os labirintos pela ordem c, b, e, d
%
%-------------------------------------------------------------------------------

resolve1(Lab,(X,Y),Pos_final,Movs):-resolve1(Lab,Pos_final,(X,Y),[(i,X,Y)],Movs).
resolve1(_,Pos_final,Pos_final,Aux,Aux):-!.
resolve1(Lab,Pos_final,Pos_atual,Aux,Movs):-movs_possiveis(Lab,Pos_atual,Aux,Poss),
	escolhe(Poss,(M,X,Y),_),
	append(Aux,(M,X,Y),NovoAux)
	resolve1(Lab,Pos_final,(X,Y),NovoAux,Movs).

%-------------------------------------------------------------------------------
%
%predicado para resolver os labirintos pela ordem c, b, e, d
%
%-------------------------------------------------------------------------------

resolve2(Lab,(X,Y),Pos_final,Movs):-
	resolve2(Lab,Pos_inicial,Pos_final,(X,Y),[(i,X,Y)],Movs).
resolve2(_,_,Pos_final,Pos_final,Aux,Aux):-!.
resolve2(Lab,Pos_inicial,Pos_final,Pos_atual,Aux,Movs):-
	movs_possiveis(Lab,Pos_atual,Aux,Poss),
	ordena_poss(Poss,Poss_ord,Pos_inicial,Pos_final),
	escolhe(Poss_ord,(M,X,Y),_),
	append(Aux,(M,X,Y),NovoAux)
	resolve2(Lab,Pos_final,(X,Y),NovoAux,Movs).


%-------------------------------------------------------------------------------
%
% predicado para escolher um dos movimentos possiveis pela ordem em que aparecem
% na lista
%
%-------------------------------------------------------------------------------	
escolhe([P | R], P, R).
escolhe([P | R], E, [P | S]) :- escolhe(R, E, S).
